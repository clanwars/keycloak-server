FROM quay.io/keycloak/keycloak:25.0.4 as builder

ENV KC_HEALTH_ENABLED=true
ENV KC_METRICS_ENABLED=true
ENV KC_FEATURES=web-authn,token-exchange
ENV KC_DB=mariadb

COPY conf/quarkus.properties /opt/keycloak/conf/
COPY themes /opt/keycloak/themes/
COPY build/libs/*.jar /opt/keycloak/providers/

RUN /opt/keycloak/bin/kc.sh build 

FROM quay.io/keycloak/keycloak:25.0.4
COPY --from=builder /opt/keycloak/lib/quarkus/ /opt/keycloak/lib/quarkus/
COPY --from=builder /opt/keycloak/themes /opt/keycloak/themes
COPY --from=builder /opt/keycloak/providers /opt/keycloak/providers

WORKDIR /opt/keycloak

# for demonstration purposes only, please make sure to use proper certificates in production instead
RUN keytool -genkeypair -storepass password -storetype PKCS12 -keyalg RSA -keysize 4096 -dname "CN=server" -alias server -ext "SAN:c=DNS:localhost,IP:127.0.0.1" -keystore conf/server.keystore

ENV KC_DB_URL_HOST=<DBHOST>
ENV KC_DB_USERNAME=<DBUSERNAME>
ENV KC_DB_PASSWORD=<DBPASSWORD>
ENV KC_HOSTNAME=localhost
ENTRYPOINT ["/opt/keycloak/bin/kc.sh"]
